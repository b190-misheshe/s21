console.log("Hi");

let cities = ["Tokyo", "Manila", "Seoul", "Jakarta", "Sim"];
console.log(cities);

// * number of elements inside the array
console.log(cities.length);

// * last index inside the array
console.log(cities.length -1);

// * last element through the index
console.log(cities[cities.length -1]);

// * blank arrays
blankArr = [];
console.log(blankArr);  //returns empty array
console.log(blankArr.length);   //returns 0
console.log(blankArr[blankArr.length]);     //returns undefined

// * delete last element in the array (decrement) - any element, not just num
console.log(cities);
cities.length --;
console.log(cities);
// cities.length --;
// console.log(cities);

// ----------
let lakersLegends = ["Kobe", "Shaq", "Magic", "Kareem", "LeBron"];
console.log(lakersLegends);

lakersLegends.length ++;
console.log(lakersLegends);

// miniactivity

//  * replace last element using index
lakersLegends[5] = "Pau Gasol";
console.log(lakersLegends);

lakersLegends[lakersLegends.length] = "Anthony Davis";
console.log(lakersLegends);

// * check each element without using forEach (looping through array)
let numArray = [5,12,30,46,40];

for (let index = 0; index < numArray.length; index ++){
    if (numArray[index] % 5 === 0) {
        console.log(numArray[index] + " is divisible by 5");
    } else {
        console.log(numArray[index] + " is not divisible by 5");
    }
}

// log each element using loop
for (let index = 0; index < numArray.length; index ++){
    console.log(index);
}

// log each element - reverse
for (let index = numArray.length; index >= 0; index --){
    console.log(numArray[index]);
}

// multidimensional arrays - are useful for storing complex data structures
let chessBoard = [
    ["a1", "b1", "c2", "d1", "e1"],
    ["a2", "b2", "c3", "d2", "e2"],
    ["a3", "b3", "c4", "d3", "e3"],
    ["a4", "b4", "c5", "d4", "e4"],
    ["a5", "b5", "c6", "d5", "e5"],
];
console.log(chessBoard);

// accessing a multidimensional array
console.log(chessBoard[1][4]);
console.log("Pawn moves to " +chessBoard[2][3]);





// let numArrays = [5,18,30,48,50];

// for (let i = 0; i < numArrays.length; i ++){
//     if (numArrays[i] % 5 === 0) {
//         console.log(numArrays[i] + " is divisible by 5");
//     } else {
//         console.log(numArrays[i] + " is not divisible by 5");
//     }
// }