console.log("Hello World!");

//
let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);
console.log(typeof grades);

let computerBrands = ["Lenovo", "Dell", "Asus", "Mac", "HP", "CDR-King"];

// console.log("");
// console.log(mi);
// mixedArr[0] = "Hello World";
// console.log("Array after reassigning:");
// console.log(object);

// Section

console.log(grades);

console.log(grades[0]);
console.log(computerBrands);

console.log(computerBrands[7]);
console.log(computerBrands.length);
console.log(grades.length);

if (computerBrands.length > 5){
    console.log("We have too many suppliers. Please coordinate with the OM.");
}

// 
let lastElementIndex = computerBrands.length - 1;
console.log(computerBrands[lastElementIndex]);

// 
let fruits = ["Apple", "Mango", "Rambutan", "Lanzones", "Durian"];
console.log(fruits);
// push() - adds an element in an array
let fruitsLength = fruits.push("Mango");
// fruitsLength = fruits

fruits.push("Guava", "Avocado")
console.log(fruitsLength);

console.log("Mutated Array from Push Method");
console.log(fruits);

// fruits[6] = "Orange";
// console.log("Mutated Array from Push Method");
// console.log(fruits);

// pop (REMOVED LAST ELEMENT)
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Push Method");
console.log(fruits);

// removedFruit = fruits.pop();
// console.log(removedFruit);
// console.log("Mutated Array from Push Method");
// console.log(fruits);

// removedFruit = fruits.pop();
// console.log(removedFruit);
// console.log("Mutated Array from Push Method");
// console.log(fruits);

// pop (REMOVED LAST ELEMENT)
removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Push Method");
console.log(fruits);

// unshift (ADD ELEMENT AT THE START)
fruits.unshift("Lime", "Banana");
// fruits.unshift("Lime");
// fruits.unshift("Banana");
console.log("Mutated Array from unshift method");
console.log(fruits);

// shift (REMOVED FIRST ELEMENT)
let fruitRemoved = fruits.shift();
console.log("Mutated Array from Shift Method");
console.log(fruits);

fruits.shift();
console.log("Mutated Array from Shift Method");
console.log(fruits);

// splice (REPLACE) (insert-index and remove-element)
fruits.splice(1,2,"Lime","Cherry");
console.log("Mutated Array from Shift Method");
console.log(fruits);

// fruits.splice(2,2,"Lime","Cherry", "Strawberry");
// console.log("Mutated Array from Shift Method");
// console.log(fruits);

// splice 
fruits.splice(3);
console.log("Mutated Array from Shift Method");
console.log(fruits);

// sort (ALPHABETICAL ORDER)
fruits.sort();
console.log("Mutated Array from Shift Method");
console.log(fruits);

// reverse (ALPHABETICAL ORDER - reverse)
fruits.reverse();
console.log("Mutated Array from Shift Method");
console.log(fruits);

// NON-Mutator Methods
// are functions that do not modify the original array

let countries = ["PH", "RUS", "CH", "JPN", "USA", "KOR", "AUS", "CAN"];
// indexOf()
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: " +firstIndex);
// print 0

let secondIndex = countries.indexOf("JPN");
console.log("Result of indexOf: " +secondIndex);
// print 3

let thirdIndex = countries.indexOf("KAZ");
console.log("Result of indexOf: " +thirdIndex);
// print -1 (non-existent)

// lastIndexOf (SEARCH / COUNT FROM LAST TO FIRST ELEMENT)
countries = ["RUS", "CH", "JPN", "PH", "USA", "KOR", "AUS", "CAN", "PH"];
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastindexOf: " +lastIndex);
// print 8

// slice (NON-MUTATOR (startingIndex-retain, endingElement-element position)
let slicedArrayA = countries.slice(3);
console.log(countries);
console.log("Result of Slice: " + slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log(countries);
console.log("Result of Slice: " + slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log(countries);
console.log("Result of Slice: " + slicedArrayC);

let stringArray = countries.toString();
console.log("Result of toString: " + stringArray);
// console.log(countries);
console.log(typeof stringArray);
console.log(typeof countries);

// concat
let tasksA = ["drink HTML", "eat Javascript"];
let tasksB = ["inhale CSS", "breathe SASS"];
let tasksC = ["get GIT", "be Node"];

let tasks = tasksA.concat(tasksB);
console.log("Result of concat: ");
console.log(tasks);

let allTasks = tasksA.concat(tasksB, tasksC);
console.log("Result of concat:");
console.log(allTasks);

// join() | join(separator)
let user = ["John", "Jane", "Joe", "Jobert", "Julius"];

console.log(user.join());
console.log(user.join(""));
console.log(user.join("-"));
console.log(typeof user);
console.log(typeof user.join());

// Iteration - EACH ELEMENT IS LOG IN THE CONSOLE


allTasks.forEach(function(task){
    console.log(task);
});

let filteredTasks = [];

allTasks.forEach(function(task){
    if (task.length > 10){
        filteredTasks.push(task);
    }
})
console.log("Result of forEach: ");
console.log(filteredTasks);

// map
let numbers = [1 ,2, 3, 4, 5];

let numbersMap = numbers.map(function(number){
    return number = number;
})
console.log("Result of map: ");
console.log(numbersMap);

// every
let allValid = numbers.every(function(number){
    return(number < 3);
})
console.log("Result of every: ");
console.log(allValid);
console.log(numbers);
//output : false (not all is < 3) (boolean)

// some
let someValid = numbers.some(function(number){
    return (number < 2);
})
console.log("Result of some: ");
console.log(someValid);
console.log(numbers);
// output : true (some of them is < 2) (boolean)

// filter
let filterValid = numbers.filter(function(number){
    return (number < 3);
})
console.log("Result of filter: ");
console.log(filterValid);
console.log(numbers);